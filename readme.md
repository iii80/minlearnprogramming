<h1 align="center">
  <br>
  https://github.com/minlearn/minlearnprogramming/：minlearn的最小编程/统一学编程软硬栈选型方案。
  <br>
</h1>

这是一套我2016-2020博客的汇编集和实践库,定位与主题为最小编程/统一学编程软硬栈选型方案，分为minlearnprogramming技术文档和onekeydevdesk演示库  
《minlearnprogramming》提出了最小编程/统一学编程软硬栈方案的中心思想，及描述了onekeydevdesk的架构原理和实现  
《onekeydevdesk》是一个最小编程/统一学编程软硬栈选型方案的实现，以配合我在《minlearnprogramming》的想法。

> 什么是最小编程/统一学编程选型栈？  

> 这是一套把学习编程所有起步过程涉及到的软硬件集合到一个最小集合的工作，以求解求新手学习编程起步到基本学会，一公里之内的事情（理论与实践）。
> 作为demo的onekeydevdesk提出了一套在线安装脚本，及一套围绕虚拟机管理器为统一多OS容器核心的OS+模拟了一套全能沉浸碎片化的IDE，作为软硬栈选型方案的实现。  

架构图：


## docs

* [minlearnprogramming intro](_build/p/_pages/minlearnprogramming/)
* [minlearnprogramming toc](_build/p/_pages/toc/#vol1onekeydevdesk):
* 🌈 offline pdf edition: [minlearnprogramming](/_build/minlearnprogramming.pdf)

* [onekeydevdesk intro](_build/p/_pages/onekeydevdesk/)
* [onekeydevdesk docs](_build/p/_pages/docs/)

## blogs

* [blogs](_build/p/_pages/blogs/)

---

本项目长期保存,联系作者协助定制onekeydevdesk/onekeydevdeskos包括不限于机型适配，应用集成等。

![](/p/onekeydevdeskopen/logo123zd15sz150.png)




