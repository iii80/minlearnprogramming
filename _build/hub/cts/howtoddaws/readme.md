
这是aws的静态ip机器，C5LARGE,仅有一个nvme盘


记一次在aws单nvme盘机器的dd windows的经验
-----

因为它的盘是/dev/nvme0n1这种，它的分区是/dev/nvme0n1px，跟普通的/dev/sda,分区是/dev/sdax逻辑不同

（前者多个字符p，后者直接附加到设备名上）

还有，它的网卡名在di下不会变成eth0（uefi的计算实例在grub设置传统名称，好像都不会转化legacy nic名）

所以对脚本自动化逻辑有新的要求

手动下就用独服的那套方法改静态ip的net.bat等文件

自动的话，就

```
wget -qO- 1keydd.com/inst.sh|bash -s - -p /dev/nvme0n1 -i ens5 -n 172.31.15.74,255.255.240.0,172.31.0.1 -t http://tutu.ovh/iso/windows/dd-5kge/win10ltsc_password_1keydd.gz
```

注意这里同时指定了-p /dev/nvme0n1 -i ens5



