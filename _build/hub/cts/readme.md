
hub：
-------

+  bymodel
     +  [onekeydevdesk支持azure拉](../../_isps/howtoddaz/)
     +  [onekeydevdesk支持servarica和xen机型了](../../_isps/howtoddsr/)
     +  [onekeydevdesk支持乌龟壳拉](../../_isps/howtoddorc/)
     +  [onekeydevdesk支持ksle拉](../../_isps/howtoddks/)
     +  [1keydd支持lowmemorymode在512m的bwg进行dd，并运行pve](../../_isps/howtoddbwglowres/)
+  byisp
     +  [记一次给云筏vds做DD的成功经验](../../_isps/howtoddcloudraft/)
     +  [记一次给景安快云VPS做DD的成功经验](../../_isps/howtoddzzidc/)
     +  [记一次给ovh做dd的成功经验](../../_isps/howtoddovh/)
     +  [刚帮人DD一个ihor-hosting.ru的机器网络复杂，成功DD](../../_isps/howtoddihor-hosting/)
     +  [记一次给512M内存腾讯云做DD的成功经验](../../_isps/howtoddqcloud/)
     +  [记一次在linode上做dd的经验](../../_isps/howtoddlinode/)
     +  [记一次在gcp多盘机器的dd经验](../../_isps/howtoddgcp-md/)
     +  [记一次在aws机器的dd经验](../../_isps/howtoddaws/)
     +  [记一次在天翼云多网卡机器的dd经验](../../_isps/howtoddctyun-mn/)
+  byos
     +  [独服大盘可用，win10ltsc uefi/bios gpt二合一兼容dd镜像](../../_isps/howtoddwin102in1/)
     +  [用我的二合一镜像SYS-2-SSD-64 盲D成功了](../../_isps/howtoddSYS-2-SSD-64/)
     +  [ikoula c-memory dd windows成功](../../_isps/howtoddikoula-cmemory/)

